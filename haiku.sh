#!/bin/bash
#
# ver 0.5 haiku.sh
#

#
# green text function
#
function color_text_green(){
    echo -e "\e[32m$1\e[0m"
}
#
# red text function
#
function color_text_red(){
    echo -e "\e[31m$1\e[0m"
}

#
# function displays help message
#
function usage() {
cat << EOF

************************************
Usage:
     -h,--help                      Help
     -u,--url    <url>              Allow to specify an url. By default: https://www.gnu.org/fun/jokes/error-haiku
     -a,--all                       Print all sentences
     -l,--limit  <n>                Print 'n' sentences
     -f,--filter <word>             Print all sentences containing 'word'
     -s,--sort                      Sort sentences alphabetically
     -o,--output  <text|json|yaml>  formate the output, json or yaml (by default, output is text)
************************************

EOF
exit 1
}

#
# get all content
#
function get_content() {
 curl -s "$1" | grep -ioE "^(\ )(\W?\w.*)" | sed -e "s/^[\ ]*//" | awk '{printf("%s%s", $0, (NR%3 ? " " : "\n"))}'
}


#
# choice of options
while true; do
 #
 # Variable $FLAG for control the parameters
 #
 case "$1" in
      -u|--url )    shift; URL=$1; FLAG=true; shift
      ;;
      -a|--all )    shift; all="true";FLAG=true;
      ;;
      # Check variable "$n" int or string
      -l|--limit )  shift; [[ $1 == ?(-)+([0-9]) ]] &&  n=$1 || usage; FLAG=true; shift
      ;;
      -f|--filter ) shift; filter=$1; FLAG=true; shift
      ;;
      -s|--sort )   shift; s="sort" FLAG=true;
      ;;
      -o|--output ) shift; o=$1; FLAG=true; shift
      ;;
      -h|--help|-|[0-9a-zA-Z\@\#\$\%\^\&]* )  usage
      ;;
      *) unset FLAG; break
      ;;
 esac
done

if [ -z "$FLAG" ] && [ ! -z $1 ]
 then
  usage
fi


#
# If no default length is specified, select only 1 sentence
#
if [ -z $n ];
  then
    n=1
fi

#
# If url is not defined set default "https://www.gnu.org/fun/jokes/error-haiku"
#
if [ -z $URL ];
   then
     URL="https://www.gnu.org/fun/jokes/error-haiku"
fi
#
# call function get_content
#
content=$(get_content "$URL")

#
# Count the number of rows
#
num_all=$(echo "$content" | wc -l)

#
# filter=<word>   Print all sentences containing 'word'
#
  if [ ! -z $filter ];
   then
    all="true"
    content=$(echo "$content" | grep -i "$filter")
    #
    # If there are no sentences, display a help message.
    #
    if [ "$content" == "" ]
     then
      echo
      color_text_red "No matching"
      echo
      exit 1
    fi
  fi
#
# For all sentences
#
if [ "$all" == "true" ];
  then
   sentences=$(echo "$content")
  else
   # random of sentences
   for (( i = 1; i <= $n; i++ ))
     do
      num_random=$(shuf -i 1-$num_all -n1)
      let "num_tail=$num_all-$num_random"
      if [ $num_tail -eq 0 ]
       then
        num_tail=1;
      fi
      #
      # select only one random sentence and accumulate it into a variable "$sentences".
      #
      sentences="$sentences$(echo "$content" |  tail -n $num_tail | head -n 1)\n"
   done
fi


#
# Sort sentences
#
  if [ ! -z $s ];
   then
    #*************************************
    # awk deletes the last "\n" and sort *
    #*************************************
    sentences=$(echo -e "$sentences" |awk 'NR>1{printf "\n"} {printf $0}'| sort)
  fi

#
# Print 'n' sentences
#
  if [ ! -z $n ];
   then
    sentences=$(echo -e "$sentences" )
  fi


#
# formate the output, json or yaml (by default, output is text)
#
  if [ "$o" == "text" ] || [ -z $o ]
   then
     color_text_green "$sentences"
  elif [ "$o" == "json"  ]
    then
      while read -r line
       do
         STR="$STR""\"$line\","
      done <<< "$sentences"
      STR=$(echo "$STR" | sed 's/,$//')
      color_text_green "{\"messages\": [$STR]}"
  elif [ "$o" == "yaml"  ]
    then
      while read -r line
       do
         STR="$STR""\t- $line\n"
      done <<< "$sentences"
      color_text_green  "messages:"
      color_text_green "$STR"
  else
      usage;
  fi

